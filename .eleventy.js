const fs = require("fs");
const crypto = require('crypto');
const { convert } = require('xmlbuilder2');

const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");

const images = {};

function loadImageObject(imagePath) {
    if (images.hasOwnProperty(imagePath)) {
        return images[imagePath];
    }
    const fileType = 'svg';
    const content = fs.readFileSync(imagePath, 'utf8');
    const contentHash = crypto.createHash('md5')
        .update(content, 'utf8')
        .digest('hex')
        .substr(0, 8)
    const dir = '/assets/img'
    const path = `${dir}/${contentHash}.${fileType}`

    fs.mkdirSync('./dist' + dir, {recursive: true})
    fs.copyFileSync(imagePath, './dist' + path)

    const obj = convert(content, { format: "object" });
    const viewBox = obj.svg['@viewBox'].split(' ');
    const width = Math.ceil(Number(viewBox[2]));
    const height = Math.ceil(Number(viewBox[3]));

    return images[imagePath] = {
        path,
        content,
        contentHash,
        width,
        height
    }
}

function imagePathShortcode(path) {
    return loadImageObject(path).path;
}

function imageShortcode(path, alt, cssClass = '', lazy = true) {
    const image = loadImageObject(path);

    const loading = lazy === true ? 'loading="lazy"' : '';
    return `<img src="${image.path}" width="${image.width}" height="${image.height}" alt="${alt}" ${loading} decoding="async" class="${cssClass}" />`;
}

function mapFileType(type) {
    switch (type) {
        case 'sh':
            return 'bash';
        case 'Procfile':
            return 'yaml';
        default:
            return type;
    }
}

function sourceCodeShortCode(filename) {
    const articleFile = this.page.inputPath;
    const codeBasePath = articleFile.replace(/^\.\/src/, './code').replace(/\.md$/, '/');
    const type = filename.split('.').pop();
    const path = codeBasePath + filename;
    return `
\`\`\`${mapFileType(type)}
${fs.readFileSync(path)}
\`\`\``;
}

module.exports = function (eleventyConfig) {
    eleventyConfig.addPlugin(syntaxHighlight);

    eleventyConfig.setUseGitIgnore(false);

    eleventyConfig.addPassthroughCopy({"./src/htaccess": '.htaccess'});
    eleventyConfig.addPassthroughCopy("./src/assets/fonts");

    eleventyConfig.addNunjucksShortcode("image", imageShortcode);
    eleventyConfig.addNunjucksShortcode("imagePath", imagePathShortcode);
    eleventyConfig.addLiquidShortcode("sourceCode", sourceCodeShortCode);

    return {
        dir: {
            input: "src",
            output: "dist"
        }
    }
};
