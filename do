#!/usr/bin/env bash

set -eu -o pipefail

ensure_npm() {
  npm install
}

function prepare_ci {
  if [[ -z "${CI:=}" ]]; then return 0; fi

  mkdir -p ~/.ssh
  chmod 700 ~/.ssh

  eval "$(ssh-agent -s)"
  echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -

  echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
  chmod 644 ~/.ssh/known_hosts
}

task_serve() {
  ensure_npm

  npm run start
}

task_build() {
  ensure_npm

  npm run build
}

task_clean() {
  rm -rf dist/
}

task_deploy() {
  prepare_ci

  rsync \
    --rsh "ssh -p $SSH_PORT" \
    --archive \
    --checksum \
    --verbose \
    dist/ \
    "$SSH_USER@$SSH_HOST:"
}

usage() {
  echo "$0 serve | build | deploy | clean"
  exit 1
}

cmd="${1:-}"
shift || true
case "$cmd" in
  clean) task_clean ;;
  serve) task_serve "$@" ;;
  build) task_build ;;
  deploy) task_deploy "$@" ;;
  *) usage ;;
esac
