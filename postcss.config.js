const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
    plugins: [
        require(`tailwindcss`)({
            purge: {
                content: [
                    "./src/**/*.njk",
                    "./src/**/*.html",
                    "./src/**/*.js",
                    "./src/**/*.css",
                ],
                options: {
                    fontFace: false
                }
            },
            theme: {
                extend: {
                    lineHeight: {
                        'extra-tight': '1.1',
                    },
                    fontFamily: {
                        sans: ["Mont W05 Light", ...defaultTheme.fontFamily.sans],
                        "sans-heavy": ["Mont W05 SemiBold", ...defaultTheme.fontFamily.sans]
                    }
                },
                colors: {
                    transparent: 'transparent',
                    current: 'currentColor',
                    black: {
                        DEFAULT: "#333333"
                    },
                    white: {
                        DEFAULT: "#FFFFFF"
                    },
                    blue: {
                        light: '#e0f3f2',
                        DEFAULT: '#7bbbbf',
                        dark: "#319299"
                    }

                }
            },
            variants: {},
            plugins: [],
        }),
        require(`autoprefixer`),
        require('cssnano')({
            preset: [
                'default',
                {
                    discardComments: {
                        removeAll: true,
                    }
                },
            ]
        }),
    ],
};